package com.example.plusgamefracment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class PlusGameViewModel : ViewModel(){
    private val _score = MutableLiveData<Score>()
    val score : LiveData<Score>
        get() = _score

    private val _resultText = MutableLiveData<String>()
    val resultText : LiveData<String>
        get() = _resultText

    private val _num1 = MutableLiveData<Int>()
    val num1 : LiveData<Int>
        get() = _num1

    private val _num2 = MutableLiveData<Int>()
    val num2 : LiveData<Int>
        get() = _num2

    private val _choice1 = MutableLiveData<Int>()
    val choice1 : LiveData<Int>
        get() = _choice1

    private val _choice2 = MutableLiveData<Int>()
    val choice2 : LiveData<Int>
        get() = _choice2

    private val _choice3 = MutableLiveData<Int>()
    val choice3 : LiveData<Int>
        get() = _choice3

    private val _totalAns = MutableLiveData<Int>()
    val totalAns : LiveData<Int>
        get() = _totalAns

    fun createQuestion() {
        _num1.value = Random.nextInt(10) + 1
        _num2.value = Random.nextInt(10) + 1


        val totalAns = _num1.value!! + _num2.value!!

            val posit = Random.nextInt(3) + 1
            if (posit == 1) {
                _choice1.value = totalAns
                _choice2.value = totalAns + 1
                _choice3.value = totalAns + 2
            } else if (posit == 2) {
                _choice1.value = totalAns - 1
                _choice2.value = totalAns
                _choice3.value = totalAns + 1
            } else {
                _choice1.value = totalAns - 2
                _choice2.value = totalAns - 1
                _choice3.value = totalAns
            }

    }

    fun checkAns1() {
        if (choice1 == totalAns) {
            _resultText.value = "Correct"
//            _score.value?.correct++

        } else {
            _resultText.value  = "Wrong"
//            _score.value?.wrong++
        }
    }
    fun checkAns2() {
        if (choice2 == totalAns) {
            _resultText.value  = "Correct"
//            _score.value?.correct++

        } else {
            _resultText.value  = "Wrong"
//            _score.value?.wrong++
        }
    }
    fun checkAns3() {
        if (choice3 == totalAns) {
            _resultText.value = "Correct"
//            _score.value?.correct++

        } else {
            _resultText.value = "Wrong"
//            _score.value?.wrong++
        }
    }

    init {
        _score.value = Score()
        _resultText.value = "Please Select an Answer"
    }


}