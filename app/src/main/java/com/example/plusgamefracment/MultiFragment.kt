package com.example.plusgamefracment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.plusgamefracment.databinding.FragmentMultiBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MultiFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MultiFragment : Fragment() {
    private lateinit var binding: FragmentMultiBinding
    private var resultText = "Please Select an Answer"
    private var scoreNum = Score()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_multi, container, false)
        binding.resultText = resultText
        binding.score = scoreNum

        StartGame()
        binding.apply {
            btnNext.setOnClickListener {
                StartGame()
                txtAns.setText("Please Select an Answer")
            }
        }
        binding.btnHome.setOnClickListener{
            it.findNavController().navigate(R.id.action_multiFragment_to_startGameMenuFragment)
        }
        return binding.root
    }
    private fun StartGame() {
        binding.apply {
            val random1 = Random.nextInt(10) + 1
            textView1.setText(Integer.toString(random1))

            val random2 = Random.nextInt(10) + 1
            textView2.setText(Integer.toString(random2))

            val sum = random1 * random2
            val posit = Random.nextInt(3) + 1
            if (posit == 1) {
                btn1.setText(Integer.toString(sum));
                btn2.setText(Integer.toString(sum + 1));
                btn3.setText(Integer.toString(sum + 2));
            } else if (posit == 2) {
                btn1.setText(Integer.toString(sum - 1));
                btn2.setText(Integer.toString(sum));
                btn3.setText(Integer.toString(sum + 1));
            } else {
                btn1.setText(Integer.toString(sum - 2));
                btn2.setText(Integer.toString(sum - 1));
                btn3.setText(Integer.toString(sum));
            }

            btn1.setOnClickListener {
                if (btn1.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreNum.correct++

                } else {
                    resultText = "Wrong"
                    scoreNum.wrong++
                }
                binding.invalidateAll()
            }
            btn2.setOnClickListener {
                if (btn2.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreNum.correct++

                } else {
                    resultText = "Wrong"
                    scoreNum.wrong++
                }
                binding.invalidateAll()

            }
            btn3.setOnClickListener {
                if (btn3.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreNum.correct++

//                        amountCorrect.text =
//                            (amountCorrect.text.toString().toInt() + 1).toString()
                } else {
                    resultText = "Wrong"
                    scoreNum.wrong++
                }
                binding.invalidateAll()

            }
        }
    }
}