package com.example.plusgamefracment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.plusgamefracment.databinding.FragmentPlusBinding
import kotlinx.android.synthetic.main.fragment_plus.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusFragment : Fragment() {
    private lateinit var binding: FragmentPlusBinding
    private lateinit var plusGameViewModel: PlusGameViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_plus, container, false)
        binding.plusGameViewModel = plusGameViewModel

        StartGame()
        binding.apply {
            btnNext.setOnClickListener {
                StartGame()
//                resultText = "Please Select an Answer"
                StartGame()
            }
        }
        binding.btnHome.setOnClickListener {
            it.findNavController().navigate(R.id.action_plusFragment_to_startGameMenuFragment)
        }
        return binding.root
    }


    private fun StartGame() {


        plusGameViewModel.createQuestion()

        btn1.setOnClickListener {
            plusGameViewModel.checkAns1()
            binding.invalidateAll()
        }
        btn2.setOnClickListener {
            plusGameViewModel.checkAns2()
            binding.invalidateAll()

        }
        btn3.setOnClickListener {
            plusGameViewModel.checkAns3()
            binding.invalidateAll()

        }
    }


}


