package com.example.plusgamefracment.databinding;
import com.example.plusgamefracment.R;
import com.example.plusgamefracment.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMultiBindingImpl extends FragmentMultiBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnHome, 4);
        sViewsWithIds.put(R.id.txtTitle, 5);
        sViewsWithIds.put(R.id.textView1, 6);
        sViewsWithIds.put(R.id.textView2, 7);
        sViewsWithIds.put(R.id.textView4, 8);
        sViewsWithIds.put(R.id.btn1, 9);
        sViewsWithIds.put(R.id.btn2, 10);
        sViewsWithIds.put(R.id.btn3, 11);
        sViewsWithIds.put(R.id.txtCorrect, 12);
        sViewsWithIds.put(R.id.txtWrong, 13);
        sViewsWithIds.put(R.id.btnNext, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMultiBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private FragmentMultiBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.Button) bindings[9]
            , (android.widget.Button) bindings[10]
            , (android.widget.Button) bindings[11]
            , (android.widget.Button) bindings[4]
            , (android.widget.Button) bindings[14]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[13]
            );
        this.amountCorrect.setTag(null);
        this.amountWrong.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.txtAns.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.score == variableId) {
            setScore((com.example.plusgamefracment.Score) variable);
        }
        else if (BR.resultText == variableId) {
            setResultText((java.lang.String) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setScore(@Nullable com.example.plusgamefracment.Score Score) {
        this.mScore = Score;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.score);
        super.requestRebind();
    }
    public void setResultText(@Nullable java.lang.String ResultText) {
        this.mResultText = ResultText;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.resultText);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.example.plusgamefracment.Score score = mScore;
        int scoreCorrect = 0;
        java.lang.String javaLangStringScoreCorrect = null;
        java.lang.String javaLangStringScoreWrong = null;
        java.lang.String resultText = mResultText;
        int scoreWrong = 0;

        if ((dirtyFlags & 0x5L) != 0) {



                if (score != null) {
                    // read score.correct
                    scoreCorrect = score.getCorrect();
                    // read score.wrong
                    scoreWrong = score.getWrong();
                }


                // read ("") + (score.correct)
                javaLangStringScoreCorrect = ("") + (scoreCorrect);
                // read ("") + (score.wrong)
                javaLangStringScoreWrong = ("") + (scoreWrong);
        }
        if ((dirtyFlags & 0x6L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.amountCorrect, javaLangStringScoreCorrect);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.amountWrong, javaLangStringScoreWrong);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.txtAns, resultText);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): score
        flag 1 (0x2L): resultText
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}